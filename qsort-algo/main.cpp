#include <QCoreApplication>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QList<int> list;

    list << 3 << 5 << 8 << 9 << 2 << 18;
    qSort(list);

    foreach (int num, list) {
        qDebug() << num;
    }
    return a.exec();
}
