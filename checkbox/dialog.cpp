#include "dialog.h"
#include "ui_dialog.h"
#include <QtCore>
#include <QtGui>
#include <QMessageBox>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    ui->checkBox->setChecked(true);

    for (int i = 0; i < 10; ++i) {
        ui->comboBox->addItem(QString::number(i) + " - Item");
    }
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_clicked()
{
    if (ui->checkBox->isChecked())
    {
        QMessageBox::information(this, "Dogs", "You like dogs");
    }
    else
    {
        QMessageBox::information(this, "Dogs", "You do not like dogs");
    }
}

void Dialog::on_pushButton_2_clicked()
{
    if (ui->radioButton->isChecked())
    {
        QMessageBox::information(this, "Dogs", "Your selection is Dogs");
    }
    else if (ui->radioButton_2->isChecked())
    {
        QMessageBox::information(this, "Cats", "Your selection is Cats");
    }
    else
    {
        QMessageBox::information(this, "None", "You have no choice");
    }
}

void Dialog::on_pushButton_3_clicked()
{
    QMessageBox::information(this, "Selection", "Your selection is: " + ui->comboBox->currentText());
}
