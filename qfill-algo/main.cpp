#include <QCoreApplication>
#include <QDebug>
#include <QVector>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QVector<QString> vect(5);

    //Fills the range [begin, end) with value.
    //qFill(vect, "hello");
    qFill(vect.begin() + 1 , vect.end() - 2, "lol");

    foreach (QString item, vect) {
        qDebug() << item;
    }
    return a.exec();
}
