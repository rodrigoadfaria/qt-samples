#include "dialog.h"
#include "ui_dialog.h"
#include <QtCore>
#include <QtGui>
#include <QMessageBox>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    for (int i = 0; i < 10; ++i) {
        ui->listWidget->addItem(QString::number(i) + " - Item");
    }
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_clicked()
{
    QMessageBox::information(this, "Your choice", ui->listWidget->currentItem()->text());
    QListWidgetItem *item = ui->listWidget->currentItem();
    item->setTextColor(Qt::red);
}
