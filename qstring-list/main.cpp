#include <QCoreApplication>
#include <QDebug>
#include <QStringList>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QStringList qsList;
    QString line = "a,b,c,d,e,f,g,h,j";

    qsList = line.split(",");
    qsList.replaceInStrings("b", "batman");

    QString after = qsList.join(",");
    qDebug() << after;

    foreach (QString mqString, qsList) {
        qDebug() << mqString;
    }

    return a.exec();
}
