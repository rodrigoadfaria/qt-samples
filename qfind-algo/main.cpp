#include <QCoreApplication>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QList<int> mList;
    mList << 1 << 15 << 58 << 65 << 54 << 98 << 685;

    //Returns an iterator to the first occurrence of value in a container in the range [begin, end).
    //Returns end if value isn't found.
    QList<int>::const_iterator iter = qFind(mList.begin(), mList.end(), 685);

    if (iter != mList.end()){
        qDebug() << "I just found it: " << *iter;
    } else {
        qDebug() << ":(  Not found";
    }

    return a.exec();
}
