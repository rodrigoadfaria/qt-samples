#include <QCoreApplication>
#include <QFile>
#include <QString>
#include <QDebug>
#include <QTextStream>


void read(QString FileName)
{
    QFile mFile(FileName);
    if (!mFile.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << "Could not open file for reading";
        return;
    }

    QTextStream in(&mFile);
    QString mString = in.readAll();
    qDebug() << mString;

    mFile.close();
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QString mFileName = ":/myfiles/res-files.pro";

    read(mFileName);
    return a.exec();
}
