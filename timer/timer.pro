#-------------------------------------------------
#
# Project created by QtCreator 2015-02-24T14:25:13
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = timer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    mytimer.cpp

HEADERS += \
    mytimer.h
