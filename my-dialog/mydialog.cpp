#include "mydialog.h"
#include "ui_mydialog.h"
#include <QWidget>
#include <QtGui>
#include <QtCore>
#include <QMessageBox>

MyDialog::MyDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MyDialog)
{
    ui->setupUi(this);
}

MyDialog::~MyDialog()
{
    delete ui;
}

void MyDialog::on_pushButton_clicked()
{
    QMessageBox::information(this, "Message", ui->lineEdit->text());
}
