#include <QCoreApplication>
#include <QDebug>
#include <QLinkedList>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    // it is almos the same as list
    // the difference is: for each element you have in the list,
    // you have a pointer to the next which you can move on
    // it is very fast in insertions and deletions
    QLinkedList<int> lkList;
    lkList << 1 << 3 << 5;

    foreach (int mNumber, lkList) {
        qDebug() << mNumber;
    }

    return a.exec();
}
