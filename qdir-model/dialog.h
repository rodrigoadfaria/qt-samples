#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QtCore>
#include <QtWidgets>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::Dialog *ui;

    //This class is obsolete. It is provided to keep old source code working.
    //In the documentation, they strongly advise against using it in new code.
    //Use QFileSystemModel instead.
    QDirModel *dirModel;

};

#endif // DIALOG_H
