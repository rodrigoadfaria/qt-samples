#include <QApplication>
#include <QTCore>
#include <QtWidgets>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QWidget *window = new QWidget;
    window->setWindowTitle("My App");

    QGridLayout *layout = new QGridLayout;

    QLabel *label = new QLabel("Name: ");
    QLineEdit *txtName = new QLineEdit;

    layout->addWidget(label, 0, 0);
    layout->addWidget(txtName, 0, 1);

    QLabel *labelT = new QLabel("Age: ");
    QLineEdit *txtNameT = new QLineEdit;

    layout->addWidget(labelT, 1, 0);
    layout->addWidget(txtNameT, 1, 1);

    QPushButton *button = new QPushButton("Ok");
    layout->addWidget(button, 2, 0, 1, 2);

    window->setLayout(layout);
    window->show();
    return app.exec();
}
