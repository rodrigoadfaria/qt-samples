#include <QCoreApplication>
#include <QList>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QList<int> list, list2;

    for (int i = 0; i < 10; ++i)
    {
        list.append(i);
    }

    list2 = list;


    //LISTS AND ITERATOR
    //to remove a specific item
    list.removeOne(5);

    //using iterator
    QListIterator<int> iter(list);

    //move back to the list
    iter.toBack();
    while (iter.hasPrevious())
    {
        qDebug() << iter.previous();
        //in case you wanna know the next item, but not
        //really moving to it
        if (iter.hasPrevious())
            qDebug() << " previous by peek -> " << iter.peekPrevious();
    }

    //MUTABLE ITERATOR
    QMutableListIterator<int> mutableIter(list2);

    while (mutableIter.hasNext())
    {
        int item = mutableIter.next();
        if (item == 8)
           mutableIter.remove();

    }

    qDebug() << "\nMUTABLE ITERATOR";
    mutableIter.toFront();
    while (mutableIter.hasNext())
    {
        qDebug() << mutableIter.next();
    }
    return a.exec();
}
