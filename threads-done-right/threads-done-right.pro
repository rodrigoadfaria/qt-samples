#-------------------------------------------------
#
# Project created by QtCreator 2015-02-25T08:46:08
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = threads-done-right
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    myobject.cpp

HEADERS += \
    myobject.h
