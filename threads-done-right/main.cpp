#include <QCoreApplication>
#include <QThread>
#include "myobject.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QThread cThread;
    MyObject mObj;

    mObj.DoSetup(cThread);
    mObj.moveToThread(&cThread);

    cThread.start();

    return a.exec();
}
