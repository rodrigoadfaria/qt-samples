#include <QCoreApplication>
#include <QFile>
#include <QString>
#include <QDebug>
#include <QTextStream>

void write(QString FileName)
{
    QFile mFile(FileName);
    if (!mFile.open(QFile::WriteOnly | QFile::Text))
    {
        qDebug() << "Could not open file for writing";
        return;
    }

    QTextStream out(&mFile);
    out << "Hello world";
    mFile.flush();
    mFile.close();
}

void read(QString FileName)
{
    QFile mFile(FileName);
    if (!mFile.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << "Could not open file for reading";
        return;
    }

    QTextStream in(&mFile);
    QString mString = in.readAll();
    qDebug() << mString;

    mFile.close();
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QString mFileName = "E:/test/myfile.txt";

    write(mFileName);
    read(mFileName);
    return a.exec();
}
