#-------------------------------------------------
#
# Project created by QtCreator 2015-02-24T15:20:42
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = threads
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    mythread.cpp

HEADERS += \
    mythread.h
