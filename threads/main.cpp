#include <QCoreApplication>
#include "mythread.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    MyThread mThreadO;
    mThreadO.name = "Thread1";

    MyThread mThreadT;
    mThreadT.name = "Thread2";

    MyThread mThreadTr;
    mThreadTr.name = "Thread3";

    mThreadO.start();
    mThreadT.start();
    mThreadTr.start();

    //mThreadO.Stop = true;

    return a.exec();
}
